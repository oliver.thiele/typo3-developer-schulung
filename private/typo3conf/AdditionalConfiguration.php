<?php
declare(strict_types=1);

defined('TYPO3') or die();

use TYPO3\CMS\Core\Core\Environment;

$context = Environment::getContext();

/**
 * Development environment
 * TYPO3_CONTEXT Development
 */
if ($context->isDevelopment()) {
    $dotenv = Dotenv\Dotenv::createImmutable(dirname(__DIR__,2));
    $dotenv->load();

    $dotenv->required(['DB_DB', 'DB_USER', 'DB_PASS', 'DB_HOST', 'DB_HOST', 'TYPO3_INSTALL_TOOL', 'TYPO3_ENCRYPTION_KEY']);

    $GLOBALS['TYPO3_CONF_VARS']['BE']['debug'] = '1';
    $GLOBALS['TYPO3_CONF_VARS']['FE']['debug'] = '1';

    $GLOBALS['TYPO3_CONF_VARS']['SYS']['devIPmask'] = '*';
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['displayErrors'] = '1';
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['exceptionalErrors'] = '12290';
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['sitename'] = '[DEV] TYPO3 Developer Schulung';
}

/**
 * DDEV Development environment
 * TYPO3_CONTEXT Development/Local
 */
if ($context->isDevelopment()  && $context->__toString() === 'Development/Local') {
    $dotenv = Dotenv\Dotenv::createImmutable(dirname(__DIR__,2));
    $dotenv->load();

    $dotenv->required(['TYPO3_INSTALL_TOOL', 'TYPO3_ENCRYPTION_KEY']);
}

/**
 * Staging environment
 * TYPO3_CONTEXT Production/Staging
 */
if ($context->isProduction() && $context->__toString() === 'Production/Staging') {
    $dotenv = Dotenv\Dotenv::createImmutable(dirname(__DIR__,2), '.env.staging');
    $dotenv->load();

    $GLOBALS['TYPO3_CONF_VARS']['SYS']['sitename'] = '[Staging] TYPO3 Developer Schulung';
}


$GLOBALS['TYPO3_CONF_VARS']['BE']['installToolPassword'] = $_ENV['TYPO3_INSTALL_TOOL'];

$GLOBALS['TYPO3_CONF_VARS']['DB']['Connections']['Default']['dbname'] = $_ENV['DB_DB'];
$GLOBALS['TYPO3_CONF_VARS']['DB']['Connections']['Default']['user'] = $_ENV['DB_USER'];
$GLOBALS['TYPO3_CONF_VARS']['DB']['Connections']['Default']['password'] = $_ENV['DB_PASS'];
$GLOBALS['TYPO3_CONF_VARS']['DB']['Connections']['Default']['host'] = $_ENV['DB_HOST'];

$GLOBALS['TYPO3_CONF_VARS']['MAIL']['transport_smtp_username'] = $_ENV['SMTP_USER'];
$GLOBALS['TYPO3_CONF_VARS']['MAIL']['transport_smtp_password'] = $_ENV['SMTP_PASSWORD'];
$GLOBALS['TYPO3_CONF_VARS']['MAIL']['transport_smtp_server'] = $_ENV['SMTP_SERVER'];

$GLOBALS['TYPO3_CONF_VARS']['SYS']['encryptionKey'] = $_ENV['TYPO3_ENCRYPTION_KEY'];

/**
 * #ddev-generated: Automatically generated TYPO3 AdditionalConfiguration.php file.
 * ddev manages this file and may delete or overwrite the file unless this comment is removed.
 * It is recommended that you leave this file alone.
 */

if (getenv('IS_DDEV_PROJECT') == 'true') {
    $GLOBALS['TYPO3_CONF_VARS'] = array_replace_recursive(
        $GLOBALS['TYPO3_CONF_VARS'],
        [
            'DB' => [
                'Connections' => [
                    'Default' => [
                        'dbname' => 'db',
                        'driver' => 'mysqli',
                        'host' => 'db',
                        'password' => 'db',
                        'port' => '3306',
                        'user' => 'db',
                    ],
                ],
            ],
            // This GFX configuration allows processing by installed ImageMagick 6
            'GFX' => [
                'processor' => 'ImageMagick',
                'processor_path' => '/usr/bin/',
                'processor_path_lzw' => '/usr/bin/',
            ],
            // This mail configuration sends all emails to mailhog
            'MAIL' => [
                'transport' => 'smtp',
                'transport_smtp_encrypt' => false,
                'transport_smtp_server' => 'localhost:1025',
            ],
            'SYS' => [
                'trustedHostsPattern' => '.*.*',
                'devIPmask' => '*',
                'displayErrors' => 1
            ],
        ]
    );
}
