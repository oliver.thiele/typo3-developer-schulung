# Links

## TYPO3 Documentation

Redirects to the current documentation

* http://tsref.de
* http://tsconfig.de


## Icons

* https://docs.typo3.org/m/typo3/reference-coreapi/11.5/en-us/ApiOverview/Icon/Index.html
* https://usetypo3.com/icon-api.html


## DDEV

https://ddev.readthedocs.io/en/stable/


## TYPO3_CONTEXT

* https://usetypo3.com/application-context.html
* https://blog.marit.ag/2014/11/03/typo3-context-verstehen-und-anwenden/


## Caching Framework

https://docs.typo3.org/m/typo3/reference-coreapi/main/en-us/ApiOverview/CachingFramework/FrontendsBackends/Index.html#cache-backends


## PHP

https://www.php-fig.org/psr/
